from PIL import Image
img = Image.open("GNUUnifontDemonstration.png")
letter_h = 20
letter_w = 24
offset_x = 0
offset_y = 0
real_height = 15
real_width = 8

data = ""
letters = {}

blank = " {"
for y in range(offset_y, offset_y + real_height):
	blank += "\n"
	for x in range(offset_x, offset_x + real_width):
		blank += "1,"
blank = blank[0:-1]
blank += "},\n"

blank2 = " {"
for y in range(offset_y, offset_y + real_height):
	blank2 += "\n"
	for x in range(offset_x, offset_x + real_width):
		blank2 += "0,"
blank2 = blank2[0:-1]
blank2 += "},\n"


for j in range(0, 4):
	offset_x = 0
	for i in range(0, 7):
		data = " {"
		for y in range(offset_y, offset_y + real_height):
			data += "\n"
			for x in range(offset_x, offset_x + real_width):
				data += str(img.getpixel((x,y))) + ","
		data = data[0:-1]
		data += "},\n"
		offset_x += letter_w
		letters[i+j*7+65] = data
	offset_y += letter_h
	if j >=1: offset_y += 1

offset_y = 0
for j in range(0, 4):
	offset_x = 7
	for i in range(0, 7):
		data = " {"
		for y in range(offset_y, offset_y + real_height):
			data += "\n"
			for x in range(offset_x, offset_x + real_width):
				data += str(img.getpixel((x,y))) + ","
		data = data[0:-1]
		data += "},\n"
		offset_x += letter_w
		letters[i+j*7+97] = data
	offset_y += letter_h
	if j >=1: offset_y += 1


offset_x = 0
offset_y = 83
for j in range(0, 10):
	data = " {"
	for y in range(offset_y, offset_y + real_height):
		data += "\n"
		for x in range(offset_x, offset_x + real_width):
			data += str(img.getpixel((x,y))) + ","
	data = data[0:-1]
	data += "},\n"
	letters[48 + ((j+1) % 10)] = data
	offset_x += 16

offset_y += 21

offset_x = 0
inds = [33,64,35,36,37, 94, 38, 42, 40, 41]
for j in range(0, 10):
	data = " {"
	for y in range(offset_y, offset_y + real_height):
		data += "\n"
		for x in range(offset_x, offset_x + real_width):
			data += str(img.getpixel((x,y))) + ","
	data = data[0:-1]
	data += "},\n"
	letters[inds[j]] = data
	offset_x += 16

offset_y += 21

offset_x = 0
inds = [59,58,39,34, 63, 91, 93, 123, 125, 43, 61]
for j in range(0, 11):
	data = " {"
	for y in range(offset_y, offset_y + real_height):
		data += "\n"
		for x in range(offset_x, offset_x + real_width):
			data += str(img.getpixel((x,y))) + ","
	data = data[0:-1]
	data += "},\n"
	letters[inds[j]] = data
	offset_x += 16

letters[32] = blank2
	
	

data = "static uint32_t letters[126][" + str(real_height*real_width) + "] = {"
for i in range(0, 126):
	if i in letters:
		data += letters[i]
	else:
		data += blank

data = data[0:-2];
data += "};"
print(data)
