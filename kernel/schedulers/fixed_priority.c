#include <stdlib.h>
#include <string.h>

#include "scheduler.h"

#include "screen.h"

#include "priority_queue.h"

// Global queue variable
pri_queue scheduler_queue;

struct Process* current = NULL;


void scheduler_init() {
    scheduler_queue = priq_new(10);
}


/**
 * Basically a (bad…) queue implemtation.
 **/
void scheduler_enqueue(struct Process* p) {
    priq_push(scheduler_queue, (void*) p, p->priority);
}


struct Process* scheduler_pop(void) {
    struct Process* process = priq_pop(scheduler_queue, NULL);

    return process;
}

struct Process* scheduler_current(void) {
	return current;
}


uint32_t scheduler_get_current_process_memory(uint32_t addr) {
	return process_get_memory_from_system(current, addr);
}


struct Process* scheduler_schedule_no_push(uint32_t* sp) {
	struct Process* next_process = scheduler_pop();

	if(next_process == NULL) {
		rpi_error("Not enough process.");
		return;
	}

    if (NULL != current) {
        // Backup the registers properly
        memcpy(&(current->registers), (void*)sp, sizeof(current->registers));
    }

	// Restore the registers for the new task
	memcpy((void*)sp, &(next_process->registers), sizeof(next_process->registers));

	// Remember which process we are on, in order to save the registers later
	current = next_process;

	return current;
}

struct Process* scheduler_schedule(uint32_t* sp) {
    if (NULL != current) {
        // Schedule the interrupted process to launch it later
        scheduler_enqueue(current);
    }

    return scheduler_schedule_no_push(sp);
}

int scheduler_get_next_id(void) {
	static int id = 0;
	id ++;
	return id;
}
