#include <stdlib.h>
#include <string.h>

#include "scheduler.h"

#include "screen.h"

#include "interrupts.h"

// Global queue variable
struct queue_node scheduler_queue = {NULL, NULL};

struct Process* current = NULL;

void scheduler_init() {
    // Disable interrupts
	RPI_GetIrqController()->Enable_Basic_IRQs = 0;
}

/**
 * Basically a (bad…) queue implemtation.
 **/
void scheduler_enqueue(struct Process* p) {
    struct queue_node* item = &scheduler_queue;
	if(current == NULL) {
		current = p;
		return;
	}

	if(scheduler_queue.self == NULL) {
		scheduler_queue.self = p;
		return;
	}

    while (NULL != item->next) {
        item = item->next;
    }

    struct queue_node* new_item = (struct queue_node*) malloc(sizeof(struct queue_node));
    new_item->self = p;
    new_item->next = NULL;

    item->next = new_item;

}

uint32_t scheduler_get_current_process_memory(uint32_t addr) {
	return process_get_memory_from_system(current, addr);
}


struct Process* scheduler_pop(void) {
    struct Process* process = scheduler_queue.self;

	if(scheduler_queue.next != NULL) {
		struct queue_node* tmp = scheduler_queue.next;
	    scheduler_queue = *(scheduler_queue.next);
		free(tmp);
	}
	else {
		scheduler_queue.next = NULL;
		scheduler_queue.self = NULL;
	}

    return process;
}

struct Process* scheduler_current(void) {
	return current;
}


struct Process* scheduler_schedule(uint32_t* sp) {
	// Schedule the interrupted process to launch it later
	scheduler_enqueue(current);

    return scheduler_schedule_no_push(sp);
}

struct Process* scheduler_schedule_no_push(uint32_t* sp) {
	struct Process* current_process = scheduler_current();
	struct Process* next_process = scheduler_pop();

	if(next_process == NULL || current_process == NULL) {
		rpi_error("Not enough process, please provide at least 2.");
		return;
	}

	// Backup the registers properly
	memcpy(&(current_process->registers), (void*)sp, sizeof(current_process->registers));

	// Restore the registers for the new task
	memcpy((void*)sp, &(next_process->registers), sizeof(next_process->registers));

	// Remember which process we are on, in order to save the registers later
	current = next_process;
}


int scheduler_get_next_id(void) {
	static int id = 0;
	id ++;
	return id;
}
