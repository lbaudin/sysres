#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct Elf32_Ehdr {
        unsigned char e_ident[16];      /* ELF identification */
        uint16_t e_type;             /* 2 (exec file) */
        uint16_t e_machine;          /* 3 (intel architecture) */
        uint32_t e_version;          /* 1 */
        uint32_t e_entry;            /* starting point */
        uint32_t e_phoff;            /* program header table offset */
        uint32_t e_shoff;            /* section header table offset */
        uint32_t e_flags;            /* various flags */
        uint16_t e_ehsize;           /* ELF header (this) size */

        uint16_t e_phentsize;        /* program header table entry size */
        uint16_t e_phnum;            /* number of entries */

        uint16_t e_shentsize;        /* section header table entry size */
        uint16_t e_shnum;            /* number of entries */

        uint16_t e_shstrndx;         /* index of the section name string table */
};

struct Elf32_Phdr {
        uint32_t p_type;             /* type of segment */
        uint32_t p_offset;
        uint32_t p_vaddr;
        uint32_t p_paddr;
        uint32_t p_filesz;
        uint32_t p_memsz;
        uint32_t p_flags;
        uint32_t p_align;
};
uint32_t read_elf_file(void* data, uint32_t dest) {
	struct Elf32_Ehdr header;

	memcpy(&header, data, sizeof(header));

	struct Elf32_Phdr phdr;

	int i = 0;

	for(i = 0; i < header.e_phnum; i++) {
		memcpy(&phdr, data + header.e_phoff + i*header.e_phentsize, sizeof(phdr));
		memcpy(((void*)dest) +phdr.p_paddr -  0x00100000, data + phdr.p_offset, phdr.p_filesz);
		char* a = "           ";
		sprintf(a, "%x, %x", phdr.p_offset, phdr.p_filesz);
		rpi_screen_draw_line(strdup(a), 0xffffff, 10+i);
	}

	return header.e_entry;
	

}

/*void _read_elf_file(const char* path) {
	FILE* f = fopen(path, "r");
	void* data = malloc(sizeof(int)*0xF000);
	printf("%d asked, %d read\n", sizeof(int)*0xf000, fread(data, sizeof(int), 0xf000, f));

	read_elf_file(data);
}*/

