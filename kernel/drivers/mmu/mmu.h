#pragma once

#include <stdint.h>

#define MMUTABLEBASE 0x00004000

void rpi_start_mmu(uint32_t a, uint32_t b);

void rpi_stop_mmu();

void rpi_mmu_invalidate_tlbs();

void rpi_mmu_section(unsigned int vadd, unsigned int padd, unsigned int flags);

void rpi_start_system_mmu();

void rpi_mmu_process_section(uint32_t ttb, unsigned int vadd, unsigned int padd, unsigned int flags);
