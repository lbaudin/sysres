#include "mmu.h"
#include "utils.h"
#include "process.h"


/**
 * Internal function to do the work for the functions below. ttb stands for 
 * Translation Table.
 **/
static void rpi_mmu_section_with_base_table(uint32_t vadd, uint32_t padd, uint32_t flags, uint32_t ttb) {
    uint32_t ra;
    uint32_t rb;
    uint32_t rc;

    ra=vadd>>20;
    rb=ttb|(ra<<2);
    ra=padd>>20;
    rc=(ra<<20)|flags|2;
    *(uint32_t*)rb = rc;
}

/** Register a correspondance between a virtual address and a physical one.
 * Only the three first figures in hexadecimal are used, since we use 1MB
 * sections here */
void rpi_mmu_section(unsigned int vadd, unsigned int padd, unsigned int flags)
{
	rpi_mmu_section_with_base_table(vadd, padd, flags, MMUTABLEBASE);
}

/** Register a correspondance between a virtual address and a physical one.
 * Only the three first figures in hexadecimal are used, since we use 1MB
 * sections here */
void rpi_mmu_process_section(uint32_t ttb, unsigned int vadd, unsigned int padd, unsigned int flags)
{
	rpi_mmu_section_with_base_table(vadd, padd, flags, ttb);
}

void rpi_start_system_mmu(void) {
	rpi_mmu_invalidate_tlbs();
	rpi_start_mmu(MMUTABLEBASE,0x00800001|0x1000|0x0004);
}

