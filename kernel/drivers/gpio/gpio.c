/** RPI sysres Kernel
 *
 * This file contains the implementation of all the functions
 * used to interact with GPIO, through the setup LED for
 * instance.
 */

// Associated header
#include "gpio.h"


// GPIO Register set
volatile rpi_gpio_t* gpio = (rpi_gpio_t*) GPIO_BASE;


volatile rpi_gpio_t* rpi_get_gpio(void) {
	return gpio;
}


void rpi_gpio_setup_led(void) {
    // Write 1 to the GPIO16 init nibble in the Function Select
    // 1 GPIO peripheral register to enable GPIO16 as an output
    rpi_get_gpio()[LED_GPFSEL] |= (1 << LED_GPFBIT);
}

void rpi_gpio_flip_led(void) {
	// Flip the LED
	static int lit = 1;
    if (lit) {
        rpi_get_gpio()[LED_GPCLR] = (1 << LED_GPIO_BIT);
        lit = 0;
    } else {
        rpi_get_gpio()[LED_GPSET] = (1 << LED_GPIO_BIT);
        lit = 1;
    }

}
