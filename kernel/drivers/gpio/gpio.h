/** RPI sysres Kernel
 *
 * This file contains the header for all the functions used to
 * interact with GPIO, through the setup LED for instance.
 */
#ifndef SYSRES_RPI_GPIO_H_
#define SYSRES_RPI_GPIO_H_

// System includes
#include <stdint.h>

/**
 * Useful constants
 */
// Base address of the GPIO register (ARM physical address)
#ifdef RPI2
    #define GPIO_BASE       0x3F200000UL
#else
    #define GPIO_BASE       0x20200000UL
#endif

#if defined(RPIBPLUS) || defined(RPI2)
    #define LED_GPFSEL      GPIO_GPFSEL4
    #define LED_GPFBIT      21
    #define LED_GPSET       GPIO_GPSET1
    #define LED_GPCLR       GPIO_GPCLR1
    #define LED_GPIO_BIT    15
#else
    #define LED_GPFSEL      GPIO_GPFSEL1
    #define LED_GPFBIT      18
    #define LED_GPSET       GPIO_GPSET0
    #define LED_GPCLR       GPIO_GPCLR0
    #define LED_GPIO_BIT    16
#endif

// GPIO function select
#define GPIO_GPFSEL0    0
#define GPIO_GPFSEL1    1
#define GPIO_GPFSEL2    2
#define GPIO_GPFSEL3    3
#define GPIO_GPFSEL4    4
#define GPIO_GPFSEL5    5

// GPIO pin output set
#define GPIO_GPSET0     7
#define GPIO_GPSET1     8

// GPIO pin out clear
#define GPIO_GPCLR0     10
#define GPIO_GPCLR1     11

// GPIO pin level
#define GPIO_GPLEV0     13
#define GPIO_GPLEV1     14

// GPIO pin event detect status
#define GPIO_GPEDS0     16
#define GPIO_GPEDS1     17

// GPIO pin rising edge detect enable
#define GPIO_GPREN0     19
#define GPIO_GPREN1     20

// GPIO pin falling edge detect enable
#define GPIO_GPFEN0     22
#define GPIO_GPFEN1     23

// GPIO pin high detect enable
#define GPIO_GPHEN0     25
#define GPIO_GPHEN1     26

// GPIO pin low detect enable
#define GPIO_GPLEN0     28
#define GPIO_GPLEN1     29

// GPIO pin async rising edge detect
#define GPIO_GPAREN0    31
#define GPIO_GPAREN1    32

// GPIO pin async falling edge detect
#define GPIO_GPAFEN0    34
#define GPIO_GPAFEN1    35

// GPIO pin pull-up/down enable
#define GPIO_GPPUD      37

// GPIO pin pull-up/down enable clock
#define GPIO_GPPUDCLK0  38
#define GPIO_GPPUDCLK1  39


// Typedef to store GPIO set
typedef uint32_t rpi_gpio_t;


/**
 * GPIO register getter
 */
volatile rpi_gpio_t* rpi_get_gpio();


/**
 * Set the setup LED
 */
void rpi_gpio_setup_led(void);

/**
 * Flip the green LED on the rpi board.
 **/
void rpi_gpio_flip_led();

#endif // SYSRES_RPI_GPIO_H_
