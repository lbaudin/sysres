#include "timer.h"

static rpi_arm_timer_t* rpiArmTimer = (rpi_arm_timer_t*)RPI_ARMTIMER_BASE;


rpi_arm_timer_t* rpi_get_timer(void)
{
    return rpiArmTimer;
}


void rpi_timer_setup(void) {

	rpi_get_timer()->Load = 0x400;


    /* Setup the ARM Timer */
    rpi_get_timer()->Control =
            RPI_ARMTIMER_CTRL_23BIT |
            RPI_ARMTIMER_CTRL_ENABLE |
            RPI_ARMTIMER_CTRL_INT_ENABLE |
            RPI_ARMTIMER_CTRL_PRESCALE_256;

}
