#include "interrupts.h"
#include "gpio.h"
#include "timer.h"
#include "screen.h"
#include "process.h"
#include "utils.h"
#include "scheduler.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


static rpi_irq_controller_t* rpiIRQController = (rpi_irq_controller_t*) RPI_INTERRUPT_CONTROLLER_BASE;


rpi_irq_controller_t* RPI_GetIrqController(void) {
	return rpiIRQController;
}


/**
 * Standard interrupt for UNDEF interrupt.
 */
void __attribute__((interrupt("UNDEF"))) undefined_instruction_vectorr(void) {
    for (;;) {
        // Do nothing
    }
}

static void swi_print(struct arm_registers registers) {

	uint32_t str = scheduler_get_current_process_memory(registers.r1);

	rpi_process_log((char*)str, scheduler_current()->id);
}


/**
 * Standard interrupt for SWI interrupt.
 */

void software_interrupt_helper(void) {

	rpi_screen_draw_line("software", 0xffcc, 4);

	/* The registers have been pushed to the stack by the assembly.
	 * the value added to _get_stack_pointer is got from the dissasembly
	 * of this function, it is the offset from the stack pointer of the assembly
	 * (the c function push some things to the stack at the beginning of the function.
	 */
	uint32_t* sp = _get_stack_pointer() + 2 + 120/4;

	struct arm_registers registers;

	memcpy(&(registers), (void*)sp, sizeof(registers));


	/* We don't really want to use dynamic allocation here. */
	char * b = "      ";
	rpi_screen_draw_line(itoa(registers.r0, b, 10), 0xcccc, 5);

	switch(registers.r0) {
	case 1:
		rpi_error((char*)registers.r1);
		break;
	case 2:
		swi_print(registers);
		break;
	case 3:  // yield
        scheduler_schedule(sp);
		break;
    case 4:  // exit
        scheduler_schedule_no_push(sp);
        break;
	default:
		break;
	}

}


/**
 * Standard interrupt for ABORT interrupt.
 */
void __attribute__((interrupt("ABORT"))) prefetch_abort_vector(void) {
	rpi_screen_draw_line("abort", 0xffcc, 4);

    for (;;) {
        // Do nothing
    }
}

/**
 * Standard interrupt for ABORT interrupt.
 */
void undef_abort_helper(uint32_t lr) {
	/* If we get here, we make the whole system crash safely. */
	rpi_screen_draw_line("undef abort", 0xffcc, 4);

	/* We absolutely don't want to use dynamic allocation here… */
	char t[25];
	itoa(lr, t, 16);
	rpi_screen_draw_line(t, 0xffcc, 5);

    for (;;) {
        // Do nothing
    }
}


/**
 * Standard interrupt for ABORT interrupt.
 */
void data_abort_helper(uint32_t lr) {
	/* If we get here, we make the whole system crash safely. */
	rpi_screen_draw_line("data abort", 0xffcc, 4);

	/* We absolutely don't want to use dynamic allocation here… */
	char t[25];
	itoa(lr, t, 16);
	rpi_screen_draw_line(t, 0xffcc, 5);

    for (;;) {
        // Do nothing
    }
}


/**
 * Standard interrupt for FIQ interrupt.
 */
void __attribute__((interrupt("FIQ"))) fast_interrupt_vector(void) {
	rpi_screen_draw_line("fiq abort", 0xffcc, 4);

    for (;;) {
        // Do nothing
    }
}


/**
 * Standard interrupt for IRQ interrupt.
 */
struct Process* interrupt_vector_helper(void) {

	/* The registers have been pushed to the stack by the assembly.
	 * the value added to _get_stack_pointer is got from the dissasembly
	 * of this function, it is the offset from the stack pointer of the assembly
	 * (the c function push some things to the stack at the beginning of the function.
	 */
	uint32_t* sp = _get_stack_pointer() + 2 + 8/4;



    // Clear the ARM Timer interrupt - it's the only interrupt we have enabled,
    // so we do not have to work out which interrupt source caused us to
    // interrupt.

	rpi_get_timer()->IRQClear = 1;

	rpi_gpio_flip_led();

	return scheduler_current();

//	return scheduler_schedule(sp);
}


void rpi_interrupts_setup(void) {
	void _enable_interrupts();

    // Enable interrupts
    _enable_interrupts();

	// Enable the timer interrupt IRQ
	RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;
}

