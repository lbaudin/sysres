typedef unsigned int emmc_arg2_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int BLKSIZE : 10;
       unsigned int reserved : 6;
       unsigned int BLKCNT : 16;
   } bitfield;
} emmc_blksizecnt_t;

typedef unsigned int emmc_arg1_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int reserved : 1;
       unsigned int TM_BLKCNT_EN : 1;
       unsigned int TM_AUTO_CMD_EN : 2;
       unsigned int TM_DAT_DIR : 1;
       unsigned int TM_MULTI_BLOCK : 1;
       unsigned int reserved2 : 10;
       unsigned int CMD_RSPNS_TYPE : 2;
       unsigned int reserved3 : 1;
       unsigned int CMD_CRCCHK_EN : 1;
       unsigned int CMD_IXCHK_EN : 1;
       unsigned int CMD_ISDATA : 1;
       unsigned int CMD_TYPE : 2;
       unsigned int CMD_INDEX : 2;
   } bitfield;
} emmc_cmdtm_t;

typedef unsigned int emmc_resp0_t;

typedef unsigned int emmc_resp1_t;

typedef unsigned int emmc_resp2_t;

typedef unsigned int emmc_resp3_t;

typedef unsigned int emmc_data_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int CMD_INHIBIT : 1;
       unsigned int DAT_INHIBIT : 1;
       unsigned int DAT_ACTIVE : 1;
       unsigned int reserved : 5;
       unsigned int WRITE_TRANSFER : 1;
       unsigned int READ_TRANSFER : 1;
       unsigned int reserved2 : 10;
       unsigned int DAT_LEVEL0 : 4;
       unsigned int CMD_LEVEL : 1;
       unsigned int DAT_LEVEL1 : 4;
       unsigned int reserved3 : 3;
   } bitfield;
} emmc_status_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int reserved : 1;
       unsigned int HCTL_DWIDTH : 1;
       unsigned int HCTL_HS_EN : 1;
       unsigned int reseverd2: 2;
       unsigned int HCTL_8BIT : 1;
       unsigned int reserved3 : 10;
       unsigned int GAP_STOP : 1;
       unsigned int GAP_RESTART : 1;
       unsigned int READWAIT_EN : 1;
       unsigned int GAP_IEN : 1;
       unsigned int SPI_MODE : 1;
       unsigned int BOOT_EN : 1;
       unsigned int ALT_BOOT_EN : 1;
       unsigned int reserved4 : 9;
   } bitfield;
} emmc_control0_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int CLK_INTLEN : 1;
       unsigned int CLK_STABLE : 1;
       unsigned int CLK_EN : 1;
       unsigned int reserved : 2;
       unsigned int CLK_GENSEL : 1;
       unsigned int CLK_FREQ_MS2 : 2;
       unsigned int CLK_FREQ8 : 8;
       unsigned int DATA_TOUNIT : 4;
       unsigned int reserved2 : 4;
       unsigned int SRST_HC : 1;
       unsigned int SRST_CMD : 1;
       unsigned int SRST_DATA : 1;
       unsigned int reserved3 ; 5;
   } bitfield;
} emmc_control1_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int CMD_DONE : 1;
       unsigned int DATA_DONE : 1;
       unsigned int BLOCK_GAP : 1;
       unsigned int reserved : 1;
       unsigned int WRITE_RDY : 1;
       unsigned int READ_RDY : 1;
       unsigned int reserved2 : 2;
       unsigned int CARD : 1;
       unsigned int reserved3 : 3;
       unsigned int RETUNE : 1;
       unsigned int BOOTACK : 1;
       unsigned int ENDBOOT : 1;
       unsigned int ERR : 1;
       unsigned int CTO_ERR : 1;
       unsigned int CCRC_ERR : 1;
       unsigned int CEND_ERR : 1;
       unsigned int CBAD_ERR : 1;
       unsigned int DTO_ERR : 1;
       unsigned int DCRC_ERR : 1;
       unsigned int DEND_EDD : 1;
       unsigned int reserved4 : 1;
       unsigned int ACMD_ERR : 1;
       unsigned int reserved5 : 7;
   } bitfield;
} emmc_interrupt_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int CMD_DONE : 1;
       unsigned int DATA_DONE : 1;
       unsigned int BLOCK_GAP : 1;
       unsigned int reserved : 1;
       unsigned int WRITE_RDY : 1;
       unsigned int READ_RDY : 1;
       unsigned int reserved2 : 2;
       unsigned int CARD : 1;
       unsigned int reserved3 : 3;
       unsigned int RETUNE : 1;
       unsigned int BOOTACK : 1;
       unsigned int ENDBOOT : 1;
       unsigned int ERR : 1;
       unsigned int CTO_ERR : 1;
       unsigned int CCRC_ERR : 1;
       unsigned int CEND_ERR : 1;
       unsigned int CBAD_ERR : 1;
       unsigned int DTO_ERR : 1;
       unsigned int DCRC_ERR : 1;
       unsigned int DEND_EDD : 1;
       unsigned int reserved4 : 1;
       unsigned int ACMD_ERR : 1;
       unsigned int reserved5 : 7;
   } bitfield;
} emmc_irpt_mask_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int CMD_DONE : 1;
       unsigned int DATA_DONE : 1;
       unsigned int BLOCK_GAP : 1;
       unsigned int reserved : 1;
       unsigned int WRITE_RDY : 1;
       unsigned int READ_RDY : 1;
       unsigned int reserved2 : 2;
       unsigned int CARD : 1;
       unsigned int reserved3 : 3;
       unsigned int RETUNE : 1;
       unsigned int BOOTACK : 1;
       unsigned int ENDBOOT : 1;
       unsigned int ERR : 1;
       unsigned int CTO_ERR : 1;
       unsigned int CCRC_ERR : 1;
       unsigned int CEND_ERR : 1;
       unsigned int CBAD_ERR : 1;
       unsigned int DTO_ERR : 1;
       unsigned int DCRC_ERR : 1;
       unsigned int DEND_EDD : 1;
       unsigned int reserved4 : 1;
       unsigned int ACMD_ERR : 1;
       unsigned int reserved5 : 7;
   } bitfield;
} emmc_irpt_en_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int ACNOX_ERR : 1;
       unsigned int ACTO_ERR : 1;
       unsigned int ACCRC_ERR : 1;
       unsigned int ACEND_ERR : 1;
       unsigned int ACBAD_ERR : 1;
       unsigned int reserved : 2;
       unsigned int NOTC12_ERR : 1;
       unsigned int reserved2 : 8;
       unsigned int UHSMODE : 3;
       unsigned int reserved3 : 3;
       unsigned int TUNEON : 1;
       unsigned int TUNED : 1;
       unsigned int reserved4 : 8;
   } bitfield;
} emmc_control2_t;


typedef union {
   unsigned int raw;
   struct {
       unsigned int CMD_DONE : 1;
       unsigned int DATA_DONE : 1;
       unsigned int BLOCK_GAP : 1;
       unsigned int reserved : 1;
       unsigned int WRITE_RDY : 1;
       unsigned int READ_RDY : 1;
       unsigned int reserved2 : 2;
       unsigned int CARD : 1;
       unsigned int reserved3 : 3;
       unsigned int RETUNE : 1;
       unsigned int BOOTACK : 1;
       unsigned int ENDBOOT : 1;
       unsigned int ERR : 1;
       unsigned int CTO_ERR : 1;
       unsigned int CCRC_ERR : 1;
       unsigned int CEND_ERR : 1;
       unsigned int CBAD_ERR : 1;
       unsigned int DTO_ERR : 1;
       unsigned int DCRC_ERR : 1;
       unsigned int DEND_EDD : 1;
       unsigned int reserved4 : 1;
       unsigned int ACMD_ERR : 1;
       unsigned int reserved5 : 7;
   } bitfield;
} emmc_force_irpt_t;

typedef unsigned int boot_timeout_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int SELECT : 1;
       unsigned int reserved : 31;
   } bitfield;
} emmc_dbg_sel_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int RD_THRSH : 1;
       unsigned int reserved : 31;
   } bitfield;
} emmc_exrdfifo_cfg_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int ENABLE : 1;
       unsigned int reserved : 31;
   } bitfield;
} emmc_exrdfifo_en_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int DELAY : 3;
       unsigned int reserved : 29;
   } bitfield;
} emmc_tune_step_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int STEPS : 6;
       unsigned int reserved : 26;
   } bitfield;
} emmc_tune_step_std_t;

typedef union {
   unsigned int raw;
   struct {
       unsigned int STEPS : 6;
       unsigned int reserved : 26;
   } bitfield;
} emmc_tune_step_ddr_t;

typedef union {
    unsigned int raw;
    struct {
        unsigned int SELECT : 8;
        unsigned int reserved : 24;
    } bitfield;
} emmc_spi_int_spt_t;

typedef union {
    unsigned int raw;
    struct {
        unsigned int SLOT_STATUS : 8;
        unsigned int reserved : 8;
        unsigned int SDVERSION : 8;
        unsigned int VENDOR : 8;
    } bitfield;
} emmc_slotirs_ver_t;


// EMMC register, located at EMMC_BASE_ADDRESS
typedef struct {
    emmc_arg2_t ARG2;
    emmc_blksizecnt_t BLKSIZECNT;
    emmc_arg1_t ARG1;
    emmc_cmdtm_t CMDTM;
    emmc_resp0_t RESP0;
    emmc_resp1_t RESP1;
    emmc_resp2_t RESP2;
    emmc_resp3_t RESP3;
    emmc_data_t DATA;
    emmc_status_t STATUS;
    emmc_control0_t CONTROL0;
    emmc_control1_t CONTROL1;
    emmc_interrupt_t INTERRUPT;
    emmc_irpt_mask_t IRPT_MASK;
    emmc_irpts_en_t IRPT_EN;
    emmc_control2_t CONTROL2;
    unsigned int reserved[4];
    emmc_force_irpt_t FORCE_IRPT 0x50;
    unsigned int reserved2[7];
    emmc_boot_timeout_t BOOT_TIMEOUT 0x70;
    emmc_dbg_sel_t DBG_SEL 0x74;
    unsigned int reserved3[2];
    emmc_exrdfifo_cfg_t EXRDFIFO_CFG 0x80;
    emmc_exrdfifo_en_t EXRDFIFO_EN 0x84;
    emmc_tune_step_t TUNE_STEP 0x88;
    emmc_tune_steps_dtd_t TUNE_STEPS_DTD 0x8C;
    emmc_tune_steps_ddr_t TUNE_STEPS_DDR 0x90;
    unsigned int reserved4[23];
    emmc_spi_int_spit_t SPI_INT_SPIT 0xF0;
    unsigned int reserved5[2];
    emmc_slotisr_ver_t SLOTISR_VER 0xFC} emmc;


// Define emmc
volatile emmc_t * const emmc = (emmc_t *) 0x7E300000;
