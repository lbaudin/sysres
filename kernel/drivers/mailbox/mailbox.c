#include "mailbox.h"

static rpi_mailbox_t* rpi_mailbox = (rpi_mailbox_t*)MAILBOX_BASE;

rpi_mailbox_t* rpi_get_mailbox(void) {
	return rpi_mailbox;
}

void rpi_mailbox_send_message(uint32_t to_send, uint32_t mailbox) {
	to_send = to_send + 0x40000000;
	while((rpi_mailbox->Status & 0x80000000) != 0) {
	}
	rpi_mailbox->Write = to_send + mailbox;
}

uint32_t rpi_mailbox_receive_message(uint32_t mailbox) {
	while((rpi_mailbox->Status & 0x40000000) != 0) {
	}
	if((rpi_mailbox->Read & 0xf) == mailbox) {
		return (rpi_mailbox->Read >> 4);
	}
	else {
		return rpi_mailbox_receive_message(mailbox);
	}
}
