#include <stdint.h>
#define MAILBOX_BASE 0x2000B880
#define MAILBOX_GPU 1

typedef struct {
 
    volatile uint32_t Read;
    volatile uint32_t Unknown1;
    volatile uint32_t Unknown2;
    volatile uint32_t Unknown3;
    volatile uint32_t Poll;
    volatile uint32_t Sender;
    volatile uint32_t Status;
    volatile uint32_t Configuration;
    volatile uint32_t Write;
    } rpi_mailbox_t;

rpi_mailbox_t* rpi_get_mailbox();
void rpi_mailbox_send_message(uint32_t to_send, uint32_t mailbox);
uint32_t rpi_mailbox_receive_message(uint32_t mailbox);
