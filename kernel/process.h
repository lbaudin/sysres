#ifndef SYSRES_RPI_PROCESS_H_
#define SYSRES_RPI_PROCESS_H_

#include <stdint.h>


// ttb stands for Translation Table Base. ttb0 is used for user process, as
// does Linux.
// TTBC is the translation table base control register.
// See https://stackoverflow.com/questions/14460752/linux-kernel-arm-translation-table-base-ttb0-and-ttb1.

// Structure to store the state of all the ARM registers.
struct arm_registers {
    uint32_t lr2;
    uint32_t sp;
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t ip;
    uint32_t lr;
};


// Default priority for newly created process
#define PROCESS_DEFAULT_PRIORITY 0;


// Struct to hold process information
struct Process {
    unsigned int id;  // Unique identifier of this process
	unsigned int active;
    char* name;  // A friendly name for this process
    int priority;  // Priority of this process
    struct arm_registers registers;  // Saved registers state
	uint32_t ttb;
    // TODO: Memory management
};

/**
 * Create a new process calling function entry_function with associated name
 * provided.
 */
struct Process* process_create(void* entry_function, char* name, int priority);

/**
 * Frees all the memory associated to the given Process.
 */
void process_delete(struct Process* p);

void process_start_mmu(struct Process* p);
void process_set_pc(struct Process* p, uint32_t pc);
uint32_t process_get_memory_from_system(struct Process* p, uint32_t addr);
struct Process* process_create_from_data(void* entry_point, char* name, int priority);

void process_yield(void);

#endif  // SYSRES_RPI_PROCESS_H_
