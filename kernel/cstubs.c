/** RPI sysres Kernel
 *
 * This file contains the stubs implementation. Stubs are primary system
 * routines, they are the minimal functionalities required to allow libc to
 * link.
 *
 * See https://sourceware.org/newlib/libc.html#Stubs for further
 * information on the c-library stubs.
 */


// System includes
#include <errno.h>
#include <stdio.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/types.h>

// Local includes
#include "screen.h"


/**
 * Disable errno macro and use our own implementation
 */
#undef errno
extern int errno;


/**
 * A helper function written in assembly to aid us in allocating memory
 */
extern caddr_t _get_stack_pointer(void);

/**
 * Close a file.
 */
int _close (int file) {
    // TODO
    return -1;
}


/**
 * A pointer to a list of environment variables and their values.
 */
char *__env[1] = {0};  // TODO
char **environ = __env;


/**
 * Transfer control to a new process.
 */
int _execve(char *name, char **argv, char** env) {
    // TODO
    errno = ENOMEM;
    return -1;
}


/**
 * Create a new process.
 */
int _fork(void) {
    // TODO
    errno = EAGAIN;
    return -1;
}


/**
 * Status of an open file.
 */
int _fstat(int file, struct stat *st) {
    // TODO
    st->st_mode = S_IFCHR;
    return 0;
}


/**
 * Process-ID.
 */
int _getpid(void) {
    // TODO
    return -1;
}


/**
 * Query whether output stream is a terminal.
 */
int _isatty(int file) {
    // TODO
    return 1;
}


/**
 * Send a signal.
 */
int kill(int pid, int sig) {
    // TODO
    errno = EINVAL;
    return -1;
}


/**
 * Establish a new name for an existing file.
 */
int _link(char *old, char *new) {
    // TODO
    errno = EMLINK;
    return -1;
}


/**
 * Set position in a file.
 */
int _lseek(int file, int ptr, int dir) {
    // TODO
    return 0;
}


/**
 * Open a file
 */
int _open(const char *name, int flags, int mode) {
    // TODO
    return -1;
}


/**
 * Read from a file.
 */
int _read(int file, char *ptr, int len) {
    // TODO
    return 0;
}


/**
 * Increase program data space. As malloc and related functions depend on this,
 * it is useful to have a working implementation. The following suffices for a
 * standalone system; it exploits the symbol _end automatically defined by the
 * GNU linker.
 */
caddr_t _sbrk(int incr) {
    extern char _end;
    static char *heap_end = 0;
    char *prev_heap_end;

    if (0 == heap_end) {
        heap_end = &_end;
    }

    prev_heap_end = heap_end;

    if ((heap_end + incr) > _get_stack_pointer()) {
        rpi_error("Out of memory");
        for (;;) {
            /* TRAP HERE! */
        }
    }

    heap_end += incr;
    return (caddr_t) prev_heap_end;
}


/**
 * Status of a file (by name).
 */
int _stat(char *file, struct stat *st) {
    // TODO
    st->st_mode = S_IFCHR;
    return 0;
}


/**
 * Timing information for current process.
 */
int _times(struct tms *buf) {
    // TODO
    return -1;
}


/**
 * Remove a file's directory entry.
 */
int _unlink(char *name) {
    // TODO
    errno = ENOENT;
    return -1;
}


/**
 * Wait for a child process.
 */
int _wait(int *status) {
    // TODO
    errno = ECHILD;
    return -1;
}


/**
 * Write to a file.
 */
int _write(int file, char *ptr, int len) {
    // TODO
    return len;
}

void yield(void) {
	syscall_test(3, NULL);
}
