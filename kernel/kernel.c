/** RPI sysres Kernel
 *
 * This file contains the main kernel code.
 */

// System includes
#include <stdlib.h>

// Local includes
#include "drivers/gpio/gpio.h"
#include "mailbox.h"
#include "drivers/interrupts/interrupts.h"
#include "timer.h"
#include "screen.h"
#include "mmu.h"
#include "utils.h"
#include "process.h"
#include "scheduler.h"
#include <stdio.h>
#include <string.h>
#include "elf.h"

/**
 * This calls some assembly function that setup the environment for
 * the first process, and then switch to user mode.
 **/
void switch_to_user_mode(void);

/**
 * PID #1, it is just draws on the screen.
 **/
void task1(void) {
	while(1) {
		rpi_screen_draw_frame();
		yield();
	}
}



/**
 * Main function - we'll never return from here.
 */
int kernel_main(void) {

	rpi_mmu_section(0x00000000,0x00000000,0x0000);

	// hardware mapping
    rpi_mmu_section(0x20000000,0x20000000,0x0000);
    rpi_mmu_section(0x20200000,0x20200000,0x0000);

	// stack, may not be useful anymore
    rpi_mmu_section(0x03f00000,0x03f00000,0x0000); // irq stack

	// screen variables
	/* The two first lines are for the emulator, and the two second for the 
	 * actual raspi. Yes, they are not the same, and it is a pain to debug.
	 **/
	rpi_mmu_section(0x5c100000,0x5c100000,0x0000);
	rpi_mmu_section(0x5c200000,0x5c200000,0x0000);
    rpi_mmu_section(0x5ea00000,0x5ea00000,0x0000);
    rpi_mmu_section(0x5eb00000,0x5eb00000,0x0000);

	// interrupt stack
	rpi_mmu_section(0xfff00000,0x00200000,0x0000);
	rpi_mmu_section(0xe5900000,0xe5900000,0x0000);
	rpi_mmu_section(0x07f00000,0x07f00000,0x0000);

	// system stack
	rpi_mmu_section(0x00200000,0x00200000,0x0000);
	rpi_mmu_section(0x07100000,0x07100000,0x0000);

	rpi_mmu_invalidate_tlbs();
    // Init the setup LED
	rpi_gpio_setup_led();

    // Init the screen
	rpi_screen_setup_no_mmu();
	rpi_start_system_mmu();
	rpi_screen_setup();

    // Initialize scheduler
    scheduler_init();

    // Booting messages
	rpi_error("Boot");
	rpi_screen_draw_frame();


	/* Binaries bundled with our kernel. Basically they are ELF files. */
	extern uint32_t _binary_test1_bin_start;
	extern uint32_t _binary_test2_bin_start;
	extern uint32_t _binary_process_exit_bin_start;
	extern uint32_t _binary_process_computation_bin_start;

	struct Process* p = process_create(task1, "test2", 2);

	/* PID #1 draws to the screen, so it must have the access to it memory */
    rpi_mmu_process_section(p->ttb, 0x5ea00000,0x5ea00000,0x0000);
    rpi_mmu_process_section(p->ttb, 0x5eb00000,0x5eb00000,0x0000);
	
	process_create_from_data(&_binary_process_computation_bin_start, "process_with_computation", 0);
	process_create_from_data(&_binary_test2_bin_start, "test1", 2);

	rpi_error("Process initialized");


	rpi_screen_draw_frame();

	rpi_gpio_flip_led();


    // Init timers
	rpi_timer_setup();

    // Init interrupts
	rpi_interrupts_setup();

	process_start_mmu(p);
	switch_to_user_mode();
}
