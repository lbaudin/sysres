#pragma once
#include <stdint.h>

void PUT32(uint32_t to, uint32_t data);
uint32_t GET32(uint32_t from);

uint32_t* _get_stack_pointer();
