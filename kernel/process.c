#include <stdlib.h>
#include <string.h>

#include "process.h"
#include "scheduler.h"
#include "mmu.h"
#include "elf.h"

/**
 * Main file for process.
 *
 * The stack is at 0xfffffffb (virtual adress, as seen by the process),
 * and 0x07f00000 + 0x00100000*pid (physical address).
 * TODO: heap
 **/
int process_init_registers(struct Process* p) {
    p->registers.sp = 0xfffffff8;
	return 0;
}

void process_set_pc(struct Process* p, uint32_t pc) {
    p->registers.lr = pc;
}



int process_init_memory(struct Process* p) {
    // TODO
    return 0;
}

/**
 * Init the MMU, memory management unit. This sets up the translation table of the process,
 * and adds some things in the system translation table to avoid crashs in the interrupts.
 **/
int process_init_mmu(struct Process* p) {
	p->ttb = (p->id+1)*0x24000;

	// disable these for now, they could be added again if there are weird bugs on the pi
	//rpi_mmu_section(0x00200000 + 0x00100000*p->id,0x00200000 + 0x00100000*(p->id+1),0x0000);
	// system stack -- probably not needed anymore
	// rpi_mmu_process_section(p->ttb, 0x00200000,0x00200000,0x0000);
	// stack, may not be useful anymore
    // rpi_mmu_process_section(p->ttb, 0x03f00000,0x03f00000,0x0000); //NOT CACHED! // irq stack
	// apparently some static variables from the kernel, not sure why they are there
	rpi_mmu_process_section(p->ttb, 0x5c100000,0x5c100000,0x0000); //NOT CACHED!
    rpi_mmu_process_section(p->ttb, 0x5c200000,0x5c200000,0x0000); //NOT CACHED!



	// Set up the stack
	rpi_mmu_process_section(p->ttb, 0xfff00000,0x07f00000 +0x00100000*(p->id),0x0000);
	rpi_mmu_process_section(p->ttb, 0x00100000,0x06f00000 +0x00100000*(p->id),0x0000);
	rpi_mmu_section(0x06f00000 +0x00100000*(p->id),0x06f00000 +0x00100000*(p->id),0x0000);

	// system stack
	rpi_mmu_process_section(p->ttb, 0x07f00000,0x07f00000, 0x0000);

	// Kernel code, needed, but permission should be investigated TODO
	rpi_mmu_process_section(p->ttb, 0x00000000,0x00000000,0x0000);

	// hardware mapping, probably not needed, or at least permissions should be investigated TODO
    rpi_mmu_process_section(p->ttb, 0x20000000,0x20000000,0x0000); //NOT CACHED!
    rpi_mmu_process_section(p->ttb, 0x20200000,0x20200000,0x0000); //NOT CACHED!

	return 0;

}

/**
 * The address must be in the .text section of the program.
 **/
uint32_t process_get_memory_from_system(struct Process* p, uint32_t addr) {

	return addr - 0x00100000 +0x06f00000 +0x00100000*(p->id);

}

/**
 * Use the process MMU table, so as the stack is the good one, and the permissions are
 * correct. Must be launched in priviliged mode.
 * It is mainly used when resuming a process, and in the main function to launch pid #1.
 **/
void process_start_mmu(struct Process* p) {
	rpi_start_mmu(p->ttb, 0x00800001|0x1000|0x0004);
}

/**
 * Init the process, regardless of it spcecifics (i.e. no code loading there). This is
 * an internal function.
 **/
struct Process* process_init(char* name) {
    struct Process* p = (struct Process*) malloc(sizeof(struct Process));
    p->id = scheduler_get_next_id();
    p->active = 0;

	// FIXME: strdup sufficient ?
    p->name = (char*)malloc(strlen(name));
    strcpy(name, p->name);

    p->priority = PROCESS_DEFAULT_PRIORITY;

    process_init_registers(p);
	process_init_mmu(p);

    process_init_memory(p);

	return p;
}


/**
 * Create process, this is the function that should be called by external code.
 **/
struct Process* process_create(void* entry_function, char* name, int priority) {
    struct Process* p = process_init(name);
    p->priority = priority;

	p->registers.lr = (uint32_t)entry_function;

	// Add the process to the scheduler list
    scheduler_enqueue(p);

    return p;
}

struct Process* process_create_from_data(void* entry_point, char* name, int priority) {
    struct Process* p = process_init(name);
    p->priority = priority;
	uint32_t start = read_elf_file(entry_point, 0x06f00000 +0x00100000*(p->id));
	process_set_pc(p, start);

	scheduler_enqueue(p);

	return p;
}


/**
 * Delete the process. Not implemented yet.
 **/
void process_delete(struct Process* p) {
    free(p->name);
    free(p);
}

