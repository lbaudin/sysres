/* from http://people.sc.fsu.edu/~jburkardt/c_src/mandelbrot_ascii/mandelbrot_ascii.html */
int main(void) {
    float r,i,R,I,b;
    char a[300];
    int index = 0, n= 0;
    for(index = 0; index < sizeof(a); index++) a[index] = '\0';
    index = 0;
    for(i=-1; i<1; i+=.1,printf("%s\n", a), yield(), index = 0)
        for(r=-2;I=i,(R=r)<1;r+=.06,a[index] = (char)(n+32), index++)
            for(n=0;b=I*I,26>n++&&R*R+b<4;I=2*R*I+i,R=R*R-b+r)
                ;
	

	printf("\n\ñFractal drawn, we can exit now.");
	return 0;
}

