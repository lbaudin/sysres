.text
.globl _start
_start:
	bl main
	bl _exit

.globl inter
inter:
	swi 10
	mov pc, lr

.globl syscall_test
syscall_test:
swi 12
mov pc, lr

.global _get_stack_pointer
_get_stack_pointer:
    // Return the stack pointer value
    str     sp, [sp]
    ldr     r0, [sp]
 
    // Return from the function
    mov     pc, lr

