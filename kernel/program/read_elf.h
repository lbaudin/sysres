#include <stdint.h>

struct Elf32_Ehdr {
        unsigned char e_ident[16];      /* ELF identification */
        uint16_t e_type;             /* 2 (exec file) */
        uint16_t e_machine;          /* 3 (intel architecture) */
        uint32_t e_version;          /* 1 */
        uint32_t e_entry;            /* starting point */
        uint32_t e_phoff;            /* program header table offset */
        uint32_t e_shoff;            /* section header table offset */
        uint32_t e_flags;            /* various flags */
        uint16_t e_ehsize;           /* ELF header (this) size */

        uint16_t e_phentsize;        /* program header table entry size */
        uint16_t e_phnum;            /* number of entries */

        uint16_t e_shentsize;        /* section header table entry size */
        uint16_t e_shnum;            /* number of entries */

        uint16_t e_shstrndx;         /* index of the section name string table */
};

struct Elf32_Phdr {
        uint32_t p_type;             /* type of segment */
        uint32_t p_offset;
        uint32_t p_vaddr;
        uint32_t p_paddr;
        uint32_t p_filesz;
        uint32_t p_memsz;
        uint32_t p_flags;
        uint32_t p_align;
};
