#include <stdio.h>
#include "read_elf.h"
#include <stdlib.h>
void read_elf_file(void* data) {
	struct Elf32_Ehdr header;

	memcpy(&header, data, sizeof(header));

	struct Elf32_Phdr phdr;

	int i = 0;

	for(i = 0; i < header.e_phnum; i++) {
		memcpy(&phdr, data + header.e_phoff + i*header.e_phentsize, sizeof(phdr));
		printf("%x %x\n", phdr.p_paddr, phdr.p_filesz);
	}
	

}

void _read_elf_file(const char* path) {
	FILE* f = fopen(path, "r");
	void* data = malloc(sizeof(int)*0xF000);
	printf("%d asked, %d read\n", sizeof(int)*0xf000, fread(data, sizeof(int), 0xf000, f));

	read_elf_file(data);
}


int main() {
	_read_elf_file("test1.bin");
	return 0;
}
