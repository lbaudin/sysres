#include "process.h"
#include <stdio.h>

// Queue element
struct queue_node {
    struct Process* self;
    struct queue_node* next;
};


void scheduler_init();

// Enqueue a process in the scheduling queue
void scheduler_enqueue(struct Process* p);

// Pop a process from the scheduling queue
struct Process* scheduler_pop(void);

// Do the actual scheduling, called by interrupt
struct Process* scheduler_schedule(uint32_t* sp);
struct Process* scheduler_schedule_no_push(uint32_t* sp);
uint32_t scheduler_get_current_process_memory(uint32_t addr);

int scheduler_get_next_id(void);
struct Process* scheduler_current();
