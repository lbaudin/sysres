Système _bare-metal_ sur Raspberry Pi
=====================================
Lucas Baudin et Lucas Verney, 2014/2015

L'objectif de ce projet était d'écrire un petit système d'exploitation
_bare-metal_ sur Raspberry Pi. Le Raspberry Pi est une carte de développement
_low cost_ construite autour d'un processeur monocœur ARM11 cadencé à 700MHz.

Pour disposer d'une librairie standard utilisable, nous avons utilisé la
[Newlib](https://sourceware.org/newlib/) proposée par Redhat. Pour pouvoir
l'utiliser, il faut écrire quelques fonctions d'interface, listées sur leur
site, qui sont toutes implémentées dans le fichier `cstubs.c`.

Nous utilisons le compilateur `arm-eabi-none-gcc` pour compiler notre noyau et
une [version modifiée de Qemu](https://github.com/Torlus/qemu) afin de tester
sans nécessairement avoir le matériel sous la main.



## Amorçage

Le fichier `start.S` contient le point d'entrée du noyau (code assembleur de
base pour définir des valeurs utiles telles que le pointeur de pile, pour
initialiser le CPU en mode _supervisor_ et pour activer les _interrupts_). Il
appelle `_cstartup` (fonction C située dans `cstartup.c`) pour effacer la
section BSS. Cette fonction appelle à son tour `kernel_main` (fonction C située
dans `kernel.c`) qui contient le code principal du noyau.

Le système d'amorçage du Raspberry Pi est un peu particulier et simplifie
grandement la procédure d'amorçage. Quand le Raspberry Pi démarre, le CPU est
désactivé et c'est le GPU qui fonctionne, sans RAM disponible. Le GPU exécute
le programme d'amorçage de premier niveau, stocké dans une ROM sur la puce. Ce
programme va lire la carte SD et va normalement charger le programme d'amorçage
de second niveau dans le cache L2 et l'exécuter. Dans notre cas, notre noyau
est le programme d'amorçage de second niveau.


## Gestion des périphériques

### GPIOs

Les GPIOs sont des ports d'entrée / sortie numérique disponibles sur le
Raspberry Pi. Ils servent à communiquer facilement avec l'extérieur. Certains
sont reliés à des composants spécifiques. Ainsi, certains pins sont capables de
produire un signal PWM, de communiquer par liaison série, ou encore sont
connectés physiquement à une LED sur la carte, pour déboguer facilement.

L'état d'un pin se contrôle à partir d'une adresse de base, qui peut être
considérée comme un pointeur sur un entier non signé sur 32 bits. En écrivant
avec des offsets successifs, il est possible de choisir la fonction du pin
considéré ainsi que sa valeur.

Le code correspondant se situe dans `drivers/gpio/`. En particulier, nous
utilisons les GPIOs afin d'avoir une LED de _heartbeat_, pratique pour déboguer
notre noyau (fonctions `rpi_gpio_setup_led` et `rpi_gpio_flip_led`).

### Écran

Le processeur graphique et le processeur ARM communiquent par un système de
_mailbox_ sur le Raspberry Pi. Nous devons utiliser ce système de communication
pour demander au processeur graphique une adresse où écrire, qui sera un
_framebuffer_. `framebuffer.S` contient la structure de données nécessaires
pour gérer le _framebuffer_.

Cette structure est décrite en assembleur pour des raisons d'alignement : il
faut que l'adresse à laquelle elle est enregistreé se termine par un certain
nombre de 0.

La première chose à implémenter est donc un facteur, avec deux méthodes
permettant de lire un message depuis la _mailbox_ et d'y écrire un message. Le
Raspberry Pi a 7 canaux de communication par _mailbox_ avec le processeur
graphique, mais seul le premier est important pour accéder au _framebuffer_.
Ces deux méthodes sont implémentées dans les fichiers du dossier
`drivers/mailbox/`.

Nos expérimations nous ont montré que la mailbox était incompatible avec
l'utilisation du MMU (il y a certainement des plages d'adresse particulière et
non documentées à activer).


Les fichiers `screen.[h|c]` contiennent le reste des fonctions permettant
d'intéragir avec l'écran, afin d'initialiser l'écran, puis de l'effacer et d'y
écrire du texte.

### CPU

Le CPU a de nombreux modes différents. La particularité et l'intérêt de ces
modes résident dans les registres qu'ils utilisent (les registres de pile, lr,
et tous les registres spéciaux sont dupliqués pour chaque mode de façon
transparente), et dans les permissions (la translation table ne peut être changé
qu'en mode privilégié).

On peut cependant distinguer le mode `User` des autres, qui a beaucoup moins de
permissions. Il partage les registres `lr` et `sp` avec le mode `System`. Nous
avons aussi utilisé les modes `IRQ` (utilisé lors des interruptions matérielle),
et `SVR` (utilisé lors des interruptions logicielles).

### Les interruptions

Une interruption peut-être déclenchée par un périphérique matériel (par exemple
le minuteur), ou par une interruption logicielle (les appels systèmes).

Dans les deux cas, il faut sauvegarder les registres, remettre en place le MMU
du système, puis revenir au MMU du processus et restorer les registres.

Ces opérations, conceptuellement simples, présentent plusieurs difficultés. Pour
la sauvegarde des registres, il faut changer le processeur de mode pour pouvoir
accéder à `lr` et `sp` du processus, mais pas les empiler tout de suite (sinon,
on empile sur la pile du processus et pas sur la pile d'interruption !), ni
les stocker temporairement dans un registre du processus qu'on aimerait
sauvegarder.

Le changement de MMU n'est pas évident non plus : si on fait un appel de
fonction, alors, pour que cet appel puisse retourner, il faut que la pile soit
encore valide… ce qui n'est pas forcément le cas si on n'a changé de table de
translation, et donc de correspondance pour la pile. Il faut donc prendre garde
à ce que les piles systèmes soient accessibles depuis toutes les tables de
translation, sans quoi l'appareil se perd dans le code et ne peut plus en
sortir.



### MMU

Nous avons utilisé le MMU du Raspberry Pi. Il y a une table de translation par
processus, en plus d'une table pour le système. Le code, finalement assez simple
mais difficile à assembler, se trouve dans `mmu.[h|c]`. Il y a normalement deux
mécanismes sur ARM pour gérer la mémoire : les pages et les sections. Afin de
simplifier l'implémentation pour notre projet, nous n'avons considérer que les
sections. Nous n'avons pas implémenté de mécanismes de mises en cache de
celles-ci (cela aurait nécessité l'accès à une mémoire persistante, ce que nous
n'avons pas).

Le MMU définit des relations entre des préfixes de 12 bits. On crée donc une
table de translation en définissant ces relations, par exemple 0x000 -> 0x000 (les sections
systèmes), 0xfff -> 0x071 (la section de la pile), 0x001 -> 0x061 (le code et le
tas) pour un processus.

Nous avons passé un peu de temps à trouvé toutes les relations nécessaires au
fonctionnement du système (les différents périphériques, les espaces où sont
chargés le code, etc…).

Avec ce mécanisme, chaque processus dispose de sa propre pile et de son propre
tas, à la même adresse virtuelle, mais dans des sections différentes.

## Ordonnanceurs

Le système que nous avons implémenté est un micro-noyau, dans le sens où les
seuls tâches réalisées en mode système sont l'amorçage, le chargement des
programmes, et l'ordonnancement. Tous les programmes, et cela inclue le
programme qui s'occuppe de la gestion de l'écran, dispose de leur propre pile,
leur tas.

Nous avons implémenté trois ordonnanceurs différents: un ordonnanceur
coopératif et deux ordonnanceurs préemptifs. Pour choisir facilement
l'ordonnanceur utilisé, nous utilisons un lien symbolique. Tous les
ordonnanceurs disponibles sont listés dans le dossier `schedulers` et il suffit
de faire un lien symbolique de l'ordonnanceur voulu vers `scheduler.c`. Par
exemple `ln -s schedulers/round_robin.c scheduler.c`.

### Ordonnanceur coopératif par FIFO

L'ordonnanceur le plus simple que nous avons implémenté est un ordonnanceur
coopératif par FIFO. Chaque nouveau processus est ajouté à une FIFO contenant
donc tous les processus à exécuter dans leur ordre d'arrivée. Il n'y a aucun
_interrupt_ et le changement de contexte se fait donc uniquement de façon
coopérative, lorsque le processus actuellement exécuté appelle `process_yield`.


### Ordonnanceur préemptif round-robin

Nous avons également implémenté un ordonnanceur préemptif round-robin. Chaque
processus s'annonce auprès du scheduler, qui l'ajoute dans une FIFO, dans
l'ordre d'arrivée. Lors d'un appel à l'ordonnanceur, le processus en tête de la
file s'exécute. Un _interrupt_ du _timer_ a lieu à intervalles réguliers et
génère un réordonnancement. Le processus actuellement exécuté est alors empilé
à la fin de la FIFO et le processus en tête de la FIFO peut alors s'exécuter
pendant un _quantum_ de temps à son tour.


### Ordonnanceur préemptif avec priorité fixée

Finalement, nous avons implémenté un ordonnanceur préemptif avec gestion des
priorités. Le principe est identique au round-robin précédemment décrit, mais
le processus choisi à chaque réordonnancement est celui dans la file avec la
plus haute priorité.

Pour se faire, nous utilisons une liste avec priorité à la place de la FIFO,
qui supporte les opérations de _push_ et de _pop_ de l'élément avec la priorité
minimale.


## Émulateur et Raspberry Pi

L'émulateur que nous utilisons est une version modifiée de Qemu, qui n'est pas
tout à fait au point. De nombreux détails sont différents, parfois difficiles à
débugguer.

Nous avons remarquer que l'émulateur est bien plus tolérant aux erreurs qu'une
vraie carte : toute la mémoire est initialisée à 0, il accepte les instructions
indéfinies (celles requérant une arithmétique flottante, par exemple `sprintf` a
du code générique qui les utilise, et donc, même en l'utilisant avec des
entiers, il faut charger l'arithmétique flottante à l'amorçage, ce qu'un
Raspberry Pi ne fait pas par défaut).

Pour le lancer, il faut d'abord compiler QEMU :
```
./configure --python=/usr/bin/python2.7 --target-list=arm-softmmu
make
```


L'exécutable qemu-system-arm est dans `arm-softmmu/`, il faut qu'il soit dans le `$PATH`.

Puis, à la racine du projet :
```
make qemu
```



## Conclusion

Nous avons donc implémenté un système d'exploitation minimaliste pour Raspberry
Pi permettant de lancer des binaires pré-compilés (liés avec le noyau) et
d'utiliser différents ordonnanceurs. Notre système d'exploitation sait en outre
gérer certains périphériques dont l'écran (sortie analogique/HDMI du Raspberry Pi),
le MMU et les GPIOs. Nous avons également inclus le code pour gérer les
_timers_ et les _interrupts_, nécessaires pour certains ordonnanceurs, et qui
peut donc être réutilisé pour gérer des sorties PWM.

En particulier, notre système d'exploitation est structuré comme un micronoyau.
Seul l'ordonnanceur fonctionne en mode système, et tout le reste (y-compris
l'affichage à l'écran) fonctionne en mode utilisateur. Chaque processus peut
appeler des fonctions en mode système en utilisant des appels systèmes, qui se
comportent alors comme des _interrupts_ logiciels.

Nous avons préféré utiliser des binaires liés statiquement avec le noyau plutôt
que d'implémenter un chargement de binaires dynamiquement, stockés sur un
support de stockage externe. En effet, la communication avec la carte SD par
l'EMMC était relativement complexe à mettre en place, et cela ne nous a pas
semblé être la partie la plus utile pour ce projet.


## Bibliographie

Nous avons utilisé les liens ci-dessous comme source de documentation sur le
Raspberry Pi et son fonctionnement.

* https://balau82.wordpress.com/2010/12/16/using-newlib-in-arm-bare-metal-programs/
  pour l'utilisation de [Newlib](https://sourceware.org/newlib/libc.hyi()tml) comme
  bibliothèque standard.
* https://www.raspberrypi.org/wp-content/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
  pour la documentation officielle de Broadcom.
* http://lxr.free-electrons.com/source/drivers/pinctrl/pinctrl-bcm2835.c pour
  le code spécifique à la gestion des GPIOs sur le BCM2835 sous Linux.
* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0471g/Cihdhefa.html
  pour la documentation sur l'assembleur ARM.
* https://en.wikipedia.org/wiki/Scheduling_%28computing%29 pour les
  ordonnanceurs implémentés.
* http://fr.slideshare.net/eurobsdcon/andy-tanenbaum-euro-bsdcon2014v2
* [Lectures from Cambridge University on assembly for the Raspberry Pi](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/ok01.html)
* https://raspberrypi.stackexchange.com/questions/10489/how-does-raspberry-pi-boot
