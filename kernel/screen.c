#include "screen.h"

#include "mailbox.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "data/letters.c"

#define LETTER_HEIGHT 15
#define LETTER_WIDTH 8

static int screen_is_setup = 0;
char** system_logs = NULL;
uint16_t* screen_blank;

#define LOGS_COUNT SCREEN_HEIGHT/LETTER_HEIGHT

int logs_process[LOGS_COUNT];

/**
 * Setup the screen, while there is no mmu. The mailbox seems to fail with the mmu enabled…
 **/
void rpi_screen_setup_no_mmu(void) {
	rpi_mailbox_send_message(((uint32_t)&FrameBufferInfo), MAILBOX_GPU);
	if(rpi_mailbox_receive_message(1) == 0) {
		rpi_screen_clear();
		screen_is_setup = 1;
	}
	else {
		while(1) {}
	}

}

/**
 * The rest of the setup, wich can be safely done with the mmu enabled.
 **/
void rpi_screen_setup(void) {
	system_logs = malloc(sizeof(char*)*LOGS_COUNT);
	int i_syslogs;
	for(i_syslogs = 0; i_syslogs < LOGS_COUNT; i_syslogs++) {
		system_logs[i_syslogs] = malloc(sizeof(char));
		system_logs[i_syslogs][0] = '\0';
	}
	screen_blank = malloc(sizeof(uint16_t)*1024);
	int  i;
	for(i = 0; i < 1024; i++) {
		screen_blank[i] = 0;
	}
}

/**
 * Clear the whole screen (i.e. fill it with black). It is a rather slow operation,
 * so it is not done at every frame. Lines are cleared one by one to avoid visual problems.
 **/
void rpi_screen_clear(void) {
	uint32_t* frame = &FrameBufferInfo;
	uint16_t* buf = (uint16_t*)(frame[8]);

	int x;

	for(x = 0; x < 1024; x++) {
		memcpy(buf+x*768, screen_blank, sizeof(uint16_t)*768);
	}
}

/**
 * Clear the line line_number.
 **/
void rpi_screen_clear_line(int line_number) {
	uint32_t* frame = &FrameBufferInfo;
	uint16_t* buf = (uint16_t*)(frame[8]);

	int y;
	int offset_y = line_number * LETTER_HEIGHT;

	for(y = offset_y; y < offset_y + LETTER_HEIGHT; y++) {
		memcpy(buf+y*1024, screen_blank, sizeof(uint16_t)*1024);
	}
}


/**
 * Draw text on line line_number with color color. If it is too long, it is not drawn, there is
 * no wrapping.
 **/
void rpi_screen_draw_line(const char* text, uint16_t color, int line_number) {
	uint32_t* frame = &FrameBufferInfo;
	uint16_t* buf = (uint16_t*)(frame[8]);

	
	int i = 0;
	int offset_x = logs_process[line_number]*50;
	int offset_y = line_number * LETTER_HEIGHT;
	while(text[i] != '\0' && i < 100) {
		int l = text[i];
		int x;
		int y;
		if(l >= 0) {
			for(x = 0; x < LETTER_WIDTH; x++) {
				for(y = 0; y < LETTER_HEIGHT; y++) {
					buf[offset_x + x + (y + offset_y)*1024] = letters[l][x + y*LETTER_WIDTH]*color;
				}
			}
		}
		offset_x += LETTER_WIDTH;
		i++;
	}

	if(i == 100) {
		rpi_error("line too long");
	}


}

int rpi_screen_is_setup() {
	return screen_is_setup;
}

void rpi_screen_draw_frame(void) {
	static int even = 0;

	even = (even + 1) % 2;

	if(even) {
		rpi_screen_draw_line(".", 0x0F0F, 1);
	}
	int i;
	for(i = 0; i < LOGS_COUNT; i++) {
		rpi_screen_clear_line(i);
		rpi_screen_draw_line(system_logs[i], 0xffff, i);
	}
}

void rpi_error(char* text) {
	if(rpi_screen_is_setup()) {
		rpi_log(text);
	}
}

void rpi_process_log(char* text, int id) {
	if(rpi_screen_is_setup()) {
		rpi_log(text);
	}
	int i;
	for(i = 0; i < LOGS_COUNT - 1; i++) {
		logs_process[i] = logs_process[i+1];
	}
	logs_process[LOGS_COUNT-1] = id;
}

/**
 * Push a new line of log to the system logger.
 * The screen MUST be initialized…
 **/
void rpi_log(char* text) {
	int i;
	
	free(system_logs[0]);
	
	for(i = 0; i < LOGS_COUNT - 1; i++) {
		system_logs[i] = system_logs[i+1];
	}
	
	system_logs[LOGS_COUNT-1] = strdup(text);
}
