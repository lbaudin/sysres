#pragma once

#include <stdint.h>

#define SCREEN_HEIGHT 768
#define SCREEN_WIDTH 1024

extern uint32_t FrameBufferInfo;
void rpi_screen_setup();
void rpi_screen_setup_no_mmu();
void rpi_screen_draw_frame();
void rpi_screen_clear();

void rpi_error(char* text);
void rpi_log(char* text);
void rpi_screen_draw_line(const char* text, uint16_t color, int line_number);
