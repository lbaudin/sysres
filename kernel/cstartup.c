/** RPI sysres Kernel
 *
 * This file contains the main C entry point of the system. Its aim is to clear
 * the BSS section and pass the control to the kernel.
 */


// Following constants are defined by the linker
extern int __bss_start__;
extern int __bss_end__;


/**
 * Kernel main is in kernel.c file
 */
extern void kernel_main(unsigned int r0, unsigned int r1, unsigned int atags);


/**
 * Main entry point, called by assembly startup program
 */
void _cstartup(unsigned int r0, unsigned int r1, unsigned int r2) {
    int* bss = &__bss_start__;
    int* bss_end = &__bss_end__;

    // Clear the BSS section.
    // See http://en.wikipedia.org/wiki/.bss for further information on the
    // BSS section
    while (bss < bss_end) {
        *bss = 0;
        ++bss;
    }

    // Call the main code, we should never return from main...
    kernel_main(r0, r1, r2);

    // ... but if we do, safely trap here
    for (;;) {
        // Empty on purpose
    }
}
